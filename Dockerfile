FROM erlang:slim as builder

ADD qemu-*-static /usr/bin/

FROM builder

ARG TSUNG_VERSION=1.7.0
LABEL maintainers="Thomas Rodriguez <trodriguez@castelis.com>, Jay MOULIN <jaymoulin@gmail.com> <https://twitter.com/MoulinJay>"
LABEL version=${TSUNG_VERSION}

ARG UID=1000

EXPOSE 8090
EXPOSE 8091

RUN apt-get update && \ 
    apt-get install -y python perl libtemplate-perl gnuplot net-tools curl make --allow-unauthenticated && \
    curl http://tsung.erlang-projects.org/dist/tsung-${TSUNG_VERSION}.tar.gz --output /tmp/tsung-${TSUNG_VERSION}.tar.gz \
    && cd /tmp/ \
    && tar xzf ./tsung-${TSUNG_VERSION}.tar.gz \
    && cd tsung-${TSUNG_VERSION} \
    && ./configure \
    && make \
    && make install \
    && apt-get purge make curl -y \
    && rm -rf /tmp/tsung*

ADD daemon.sh /root/daemon.sh
ADD tsung-report.sh /usr/local/bin/tsung-report
WORKDIR "/root/.tsung"
CMD ["/root/daemon.sh"]
